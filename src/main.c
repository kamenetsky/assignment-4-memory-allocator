#define _GNU_SOURCE
#include <assert.h>
#include <stdio.h>
#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

#define HEAP_SIZE 4096
#define MAX_HEAP_SIZE 4096
#define INITIAL_HEAP_SIZE 10
#define BLOCK_SIZE_1 250
#define BLOCK_SIZE_2 200
#define BLOCK_SIZE_3 300
#define REGION_SIZE 8

extern void debug(const char* fmt, ... );
extern struct block_header* block_get_header(void* contents);
extern void* map_pages(void const* addr, size_t length, int flags);

void just_success_malloc(){
    debug("Test 1:\n");
    debug("Creating heap of size %d bytes\n", MAX_HEAP_SIZE);
    void* heap = heap_init(MAX_HEAP_SIZE);
    assert(heap);
    debug("Heap created, size %d bytes\n", MAX_HEAP_SIZE);
    debug("1: Allocating some bytes\n");
    debug("Allocating %d bytes\n", BLOCK_SIZE_1);
    void* block1 = _malloc(BLOCK_SIZE_1);
    assert(block1);
    debug("Allocation successful!\n");

    heap_term();
    debug("Test 1 completed!\n");
}

void alloc_two_free_one(){
    debug("Test 2:\n");
    debug("Creating heap of size %d bytes\n", MAX_HEAP_SIZE);
    void* heap = heap_init(MAX_HEAP_SIZE);
    assert(heap);
    debug("Heap initialized, size %d bytes\n", MAX_HEAP_SIZE);
    debug("1: Allocating 2 blocks\n");
    debug("\tAllocating %d bytes\n", BLOCK_SIZE_1);
    void* block1 = _malloc(BLOCK_SIZE_1);
    assert(block1);
    debug("\tAllocating %d bytes\n", BLOCK_SIZE_2);
    void* block2 = _malloc(BLOCK_SIZE_2);
    assert(block2);
    debug("2: Freeing the first block\n");
    _free(block1);
    assert(block_get_header(block1)->is_free);
    assert(!block_get_header(block2)->is_free);

    heap_term();
    debug("Test 2 completed!\n");
}

void alloc_three_free_two(){
    debug("Test 3:\n");
    debug("Creating heap of size %d bytes\n", MAX_HEAP_SIZE);
    void* heap = heap_init(MAX_HEAP_SIZE);
    assert(heap);
    debug("Heap initialized, size %d bytes\n", MAX_HEAP_SIZE);
    debug("1: Allocating 3 blocks\n");
    debug("\tAllocating %d bytes\n", BLOCK_SIZE_1);
    void* block1 = _malloc(BLOCK_SIZE_1);
    assert(block1);
    debug("\tAllocating %d bytes\n", BLOCK_SIZE_2);
    void* block2 = _malloc(BLOCK_SIZE_2);
    assert(block2);
    debug("\tAllocating %d bytes\n", BLOCK_SIZE_3);
    void* block3 = _malloc(BLOCK_SIZE_3);
    assert(block3);
    debug("2: Freeing the second and first blocks\n");
    _free(block2);
    _free(block1);
    assert(block_get_header(block1)->is_free);
    assert(block_get_header(block2)->is_free);
    assert((block_get_header(block1)->capacity.bytes == BLOCK_SIZE_2 + size_from_capacity((block_capacity){.bytes=BLOCK_SIZE_1}).bytes));
    assert(!block_get_header(block3)->is_free);

    heap_term();
    debug("Test 3 completed!\n");
}

void extend_region(){
    debug("Test 4:\n");
    debug("Creating heap of size %d bytes\n", INITIAL_HEAP_SIZE);
    void* heap = heap_init(INITIAL_HEAP_SIZE);
    assert(heap);
    debug("Heap initialized, size %d bytes\n", INITIAL_HEAP_SIZE);
    debug("1: Allocating memory\n");
    debug("Allocating %d bytes\n", BLOCK_SIZE_1);
    void* block1 = _malloc(BLOCK_SIZE_1);
    assert(block1);
    debug("Allocation successful!\n");

    heap_term();
    debug("Test 4 completed!\n");
}

void cannot_extend(){
    debug("Test 5:\n");
    debug("Allocating region\n");
    void* region = map_pages(HEAP_START, REGION_SIZE, MAP_FIXED);
    assert(region);
    debug("Allocating within occupied region\n");
    void* block_in_occupied = _malloc(BLOCK_SIZE_1);
    assert(block_in_occupied);
    assert(region != block_in_occupied);

    heap_term();
    debug("Test 5 completed!\n");
}

int main() {
    just_success_malloc();
    debug("\n");
    alloc_two_free_one();
    debug("\n");
    alloc_three_free_two();
    debug("\n");
    extend_region();
    debug("\n");
    cannot_extend();
    debug("All tests completed!\n");

    return 0;
}
